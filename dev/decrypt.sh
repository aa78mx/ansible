#!/bin/bash

toplevel=$(git rev-parse --show-toplevel)

# decrypt all vault files if they are encrypted
find $toplevel -name "vault" | xargs -L1 sudo ansible-vault decrypt --vault-password-file /root/.ansiblevault
find $toplevel -name "vault" | xargs -L1 sudo chown matthias:matthias
find $toplevel -name "vault" | xargs -L1 sudo chmod 664