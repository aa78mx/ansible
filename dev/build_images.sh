#!/bin/bash

docker login registry.gitlab.com
docker build -t registry.gitlab.com/matthiasjonen/ansible:ubuntu2004_server -f ./dockerfiles/Dockerfile_ubuntu2004_server .
docker push registry.gitlab.com/matthiasjonen/ansible:ubuntu2004_server
docker build -t registry.gitlab.com/matthiasjonen/ansible:debian_buster_server -f ./dockerfiles/Dockerfile_debian_buster_server .
docker push registry.gitlab.com/matthiasjonen/ansible:debian_buster_server
docker build -t registry.gitlab.com/matthiasjonen/ansible:arch -f ./dockerfiles/Dockerfile_arch .
docker push registry.gitlab.com/matthiasjonen/ansible:arch