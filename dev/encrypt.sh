#!/bin/bash

toplevel=$(git rev-parse --show-toplevel)

# run ansible-lint against local.yml
sudo ansible-lint -v $toplevel/local.yml

# encrypt all vault files if they are decrypted
find $toplevel -name "vault" | xargs -L1 sudo ansible-vault encrypt --vault-password-file /root/.ansiblevault
find $toplevel -name "vault" | xargs -L1 sudo chown matthias:matthias
find $toplevel -name "vault" | xargs -L1 sudo chmod 664