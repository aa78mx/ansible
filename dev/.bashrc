# .bashrc for root in docker container. Helps me to test easily.
# Path variable
export PATH=$PATH:$HOME/.local/bin

#Some more alias to avoid making mistakes:
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

#Some more alias to test my ansible provision
alias defhost='sed -i "/server/a$HOSTNAME ansible_connection=local" /opt/dev/hosts.override'
alias defworkstation='sed -i "/workstation/a$HOSTNAME ansible_connection=local" /opt/dev/hosts.override'
alias play1_local='ansible-playbook --limit $HOSTNAME --vault-password-file /root/.ansiblevault /opt/local.yml -i /opt/dev/hosts.override'

#Helpful aliases for debugging and information gathering
alias ansible_facts='ansible localhost -m setup > /opt/dev/ansible_facts'