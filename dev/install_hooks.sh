#!/bin/bash
# Installs git hooks.

TOPDIR=$(git rev-parse --show-toplevel)

rm -f $TOPDIR/.git/hooks/pre-commit
ln -s -f $TOPDIR/dev/hooks/pre-commit.py .git/hooks/pre-commit