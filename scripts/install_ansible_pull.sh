#!/bin/bash

read -p "Is this a (p)rod or (s)taging instance (p/s)?" CHOICE
case "$CHOICE" in
  p|P ) INVENTORY=production;;
  s|S ) INVENTORY=staging;;
  * ) echo "invalid";;
esac

read -p "Please specify branch name for ansible-pull" BRANCH


# create .ansiblevault password file with registry password
echo "Provide ansiblevault - secret"
IFS= read -s  -p Password: SECRET

echo "$SECRET" | install -m 600 /dev/stdin /root/.ansiblevault

# Set up ansible
mkdir -p /usr/share/ansible/plugins/modules && \
  touch /usr/share/ansible/plugins/modules/aur.py          # Dummy file for ansible-aur module
[ -f /etc/apt/sources.list.d/pve-enterprise.list ] && \
  rm /etc/apt/sources.list.d/pve-enterprise.list           # Add the no-subscription repo with playbook
[ -f /usr/bin/apt ] && apt install -y git python3-pip sudo
[ -f /usr/sbin/pacman ] && \
  pacman -S git python python-pip sudo --noconfirm && \
  echo 'export PATH=$PATH:$HOME/.local/bin' >> /root/.bashrc
pip3 install ansible

# Run Playbook
ansible-pull -U https://gitlab.com/MatthiasJonen/ansible.git --vault-password-file /root/.ansiblevault -i inventory/$INVENTORY --checkout "$BRANCH"
