#!/usr/bin/bash

read -p "Provide disk name for installation (like sda)" disk

mkfs.fat -F32 /dev/"${disk}1"
mkswap /dev/"${disk}2"
swapon /dev/"${disk}2"
mkfs.btrfs /dev/"${disk}3"

mount /dev/"${disk}3" /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@snapshots
btrfs subvolume create /mnt/@var_log

umount /mnt
mount -o noatime,compress=lzo,space_cache=v2,subvol=@ /dev/"${disk}3" /mnt
mkdir -p /mnt/{boot,home,.snapshots}
mount -o noatime,compress=lzo,space_cache=v2,subvol=@home /dev/"${disk}3" /mnt/home
mount -o noatime,compress=lzo,space_cache=v2,subvol=@snapshots /dev/"${disk}3" /mnt/.snapshots
mount -o noatime,compress=lzo,space_cache=v2,subvol=@var_log /dev/"${disk}3" /mnt/var/log
mount /dev/"${disk}1" /mnt/boot

# Prepare statements for next steps in iso:
echo 'alias pacstrap="pacstrap /mnt base linux linux-firmware neovim"'
echo 'alias genfstab="genfstab -U /mnt >> /mnt/etc/fstab"'

# Prepare statements for next steps in arch-chroot
echo 'alias 1_timezone="ln -sf /usr/share/zoneinfo/Europe/Vienna /etc/localtime"' >> /mnt/root/.zshrc
echo 'alias 2_hwclock="hwclock --systohc"' >> /mnt/root/.zshrc
echo 'alias 3_install_base="pacman -S grub efibootmgr networkmanager network-manager-applet snapper"' >> /mnt/root/.zshrc
echo 'alias 4_grub="grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB"' >> /mnt/root/.zshrc
echo 'alias 5_grub="grub-mkconfig -o /boot/grub/grub.cfg"' >> /mnt/root/.zshrc
echo 'alias 6_enable="systemctl enable NetworkManager"' >> /mnt/root/.zshrc