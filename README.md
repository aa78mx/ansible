# ansible

My ansible configuration for homelab server and workstations. Inspired by learnlinux.tv

CICD Using gitlab runner and docker first and proxmox vms in staging. This is inspired by [kruyt.org](https://kruyt.org/ansible-ci-with-gitlab/)

## Baseinstall for arch
Main source for this is [EF- Tech Made Simple](https://youtu.be/Xynotc9BKe8)
### Base setup

```
pacman -Syy
pacman -S wget
wget https://gitlab.com/MatthiasJonen/ansible/-/raw/master/scripts/install_arch_step1.sh
wget https://gitlab.com/MatthiasJonen/ansible/-/raw/master/scripts/install_arch_step2.sh
chmod +x install_arch*.sh
./install_arch_step1.sh
```
### Manually partition the disk
Manually partition the disk. In my case it is sda
`gdisk /dev/sda`
n for new partition (efi)
defaults until Last sector: +300M
Hex Code ef00

n for new partition (swap)
defaults until Last sector: +2G
Hex Code 8200

n for new partition
only defaults

w

### Formating, mounting and install base packages
`./install_arch_step2.sh`
When prompted type your disk name (like sda)

after the script:
```
pacstrap /mnt base linux linux-firmware neovim

genfstab -U /mnt >> /mnt/etc/fstab
```
or use the aliases for this...

### Chroot /mnt

`arch-chroot mnt`
Define hostname and hosts
```
nvim /etc/hostname
nvim /etc/hosts #TODO: ansible playbook
source /root/.zshrc
```
Use .zshrc aliases 1_timezone and 2_hwclock

#Change root password
`passwd`

Use alias 3_install_base
```
nvim /etc/mkinitcpio.conf
#put btfrs in MODULES
MODULES=(btrfs)
mkinitcpio -p linux

#Use aliases 4 till 6.

exit
umount -a
reboot
```


## Development Info
### Install git hooks
* To install git hooks execute the `./dev/install_hooks.sh`
* Make sure all confident information is in files named 'vault'. The following scripts help with encryption and decryption.
  ```
  ./dev/encrypt.sh
  ./dev/decrypt.sh
  ```

### Set up staging host
Become root, download the relevant bash script and run it.
```
wget https://gitlab.com/MatthiasJonen/ansible/-/raw/master/scripts/install_ansible_pull.sh
chmod +x install_ansible_pull.sh
./install_ansible_pull.sh
```